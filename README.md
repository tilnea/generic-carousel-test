## Install the dependencies:
`yarn`

## Start development:
`yarn start`

## Run tests:
`yarn test`

