const initialState = { currentImage: 0, max: 11 };

export const NEXT = 'NEXT';
export const PREV = 'PREV';

export function reducer(state = initialState, action) {

  switch ( action.type ) {
    case NEXT: 
      return state.currentImage < state.max ? { ...state, currentImage: state.currentImage + 1 } : { ...state, currentImage: 0 };
    case PREV: 
      return state.currentImage > 0 ? { ...state, currentImage: state.currentImage - 1 } : { ...state, currentImage: state.max}
    default:
      return state;
  }
}

export function next(payload) {
  return {
    type: NEXT,
    payload
  }
}

export function prev(payload) {
  return {
    type: PREV,
    payload
  }
}