import axios from 'axios';
const initialState = { images: [] };

export const LOAD_IMAGES = 'LOAD_IMAGES';

export function reducer(state = initialState, action) {
  switch ( action.type ) {
    case LOAD_IMAGES: 
      return { ...state, images: action.payload.slice(0, 12) }
    default:
      return state;
  }
}

export function getImages(payload) {
  return {
    type: LOAD_IMAGES,
    payload
  }
}

export function fetchImageList() {
  return(dispatch) => {
    return axios.get("https://picsum.photos/list").then((res) => {
      dispatch(getImages(res.data));
    });
  }
}