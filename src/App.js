import React, { Component } from 'react';
import './App.css';
import Carousel from './components/Carousel/Carousel';

class App extends Component {
  componentDidMount() {
    this.props.fetchImageList();
  }
  
  render() {
    return (
      <div className="app">
        <Carousel />
      </div>
    );
  }
}

export default App;