import React from 'react';
import ReactDOM from 'react-dom';
import Image from '../components/Image/Image';
import { Provider } from 'react-redux';
import store from '../store';
import renderer from 'react-test-renderer';
import * as Fetcher from '../action-reducers/fetcher';

describe('Default Image test', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Provider store={store}>
      <Image src={1} />
    </Provider>, div);
    ReactDOM.unmountComponentAtNode(div);
  }),
  it('renders correctly', () => {
    const tree = renderer
      .create(<Provider store={store}>
        <Image src={1} />
      </Provider>)
      .toJSON();
    expect(tree).toMatchSnapshot();
  })
});