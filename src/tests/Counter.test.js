import * as Counter from '../action-reducers/counter';

describe('Counter reducer', () => {
  it('should return the initial state og this reducer', () => {
  	expect(Counter.reducer(undefined, {})).toEqual({currentImage: 0, max: 11})
  }),
  it('should return the next value', () => {
  	const oldState = { max: 11, currentImage: 11 };
  	const nextAction = Counter.next();
  	const actualResult = Counter.reducer(oldState, nextAction);
  	const expectedResult = { max: 11, currentImage: 0 };

  	expect(actualResult).toEqual(expectedResult);
  }),
  it('should return the prev value', () => {
  	const oldState = { max: 11, currentImage: 0 };
  	const prevAction = Counter.prev();
  	const actualResult = Counter.reducer(oldState, prevAction);
  	const expectedResult = { max: 11, currentImage: 11 };

  	expect(actualResult).toEqual(expectedResult);
  })
});