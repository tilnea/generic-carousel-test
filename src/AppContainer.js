import App from './App.js';
import { connect } from 'react-redux';
import * as Fetcher from './action-reducers/fetcher';

const mapDispatchToProps = dispatch => {
  return {
    fetchImageList:(payload) => dispatch(Fetcher.fetchImageList(payload))
  };
}

export default connect(null, mapDispatchToProps)(App);