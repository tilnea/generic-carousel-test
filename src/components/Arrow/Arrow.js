import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

class Arrow extends Component {
  render() {
    const { onClick } = this.props;

    return (
      <button className="arrow" onClick={onClick}></button>
    );
  }
}

Arrow.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default Arrow;