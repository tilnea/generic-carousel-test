import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

class Button extends Component {
  render() {
    const { title, onClick } = this.props;

    return (
      <button className="button" onClick={onClick}><span className="button__title">{title}</span></button>
    );
  }
}

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
};


export default Button;