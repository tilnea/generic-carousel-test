import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

class Image extends Component {
  render() {
    const { src } = this.props;

    return (
      <div className="image">
        <img className="image__image" alt={src} src={`https://picsum.photos/520/520?image=${src}`} />
        <p>Image {src + 1}</p>
      </div>
    );
  }
}

Image.propTypes = {
  src: PropTypes.number.isRequired,
};


export default Image;