import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import store from '../../store';
import * as Counter from '../../action-reducers/counter';
import Button from '../Button/Button';
import Arrow from '../Arrow/Arrow';
import Image from '../Image/Image';

const mapStateToProps = state => {
  return {
    currentImage: state.counter.currentImage,
    images: state.fetcher.images,
  };
};

class Carousel extends Component {
  constructor(props) {
    super(props);

    //nbrImagesDisplayed is the number we want to display on screen
    this.state = { 
      nbrImagesDisplayed: 1
    }
  }

  componentDidMount() {
    window.addEventListener("resize", this.resize);
    this.resize();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.resize);
  }

  next = () => {
    store.dispatch(Counter.next());
  }

  prev = () => {
    store.dispatch(Counter.prev());
  }

  resize = (e) => {
    if ( window.innerWidth >= 720 && window.innerWidth < 900 ) {
      this.setState({ nbrImagesDisplayed: 3 });
    } else if ( window.innerWidth >= 900 ) {
      this.setState({ nbrImagesDisplayed: 5 });
    } else {
      this.setState({ nbrImagesDisplayed: 1 });
    }
  }

  renderImage = (i) => {
    return <Image key={`image-${i}`} src={this.props.images[i].id} />
  }

  renderImages = () => {
    const startIndex = this.props.currentImage;
    const endIndex = this.props.currentImage + this.state.nbrImagesDisplayed;
    const images = [];
    for (let i = startIndex; i < endIndex; i++) {
      images.push(this.renderImage(i % this.props.images.length));
    }
    return images;
  }

  render() {
    const { images } = this.props;
    const renderImages = images.length > 0 ? this.renderImages() : <div>Loading...</div>;

    //Gets the default size for images and sets the container for the arrows to that size
    const imageHeight = (window.innerWidth)/(this.state.nbrImagesDisplayed) -5;
    return (
      <div className="carousel">
        <div className="carousel__images">
        { renderImages }
        <div className="carousel__arrows" 
          style={{height: imageHeight}}>
          <Arrow onClick={this.prev} />
          <Arrow onClick={this.next} />
        </div>
        </div>
        <div className="carousel__buttons">
          <Button title='Prev' onClick={this.prev} />
          <Button title='Next' onClick={this.next} />
        </div>      
      </div>
    );
  }
}

Carousel.propTypes = {
  currentImage: PropTypes.number.isRequired,
  images: PropTypes.array.isRequired,
};

export default connect(mapStateToProps)(Carousel);