import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from "redux-thunk";

import * as Counter from '../action-reducers/counter';
import * as Fetcher from '../action-reducers/fetcher';

const rootReducer = combineReducers({ 
  counter: Counter.reducer, 
  fetcher: Fetcher.reducer
});

const store = createStore(rootReducer, applyMiddleware(thunk));
export default store;